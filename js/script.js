let $slider = $('.slider');
let slideCount = 0;
let totalSlides = 0;
let loadingProgress = 0;

// Reusable elements to create slides
let $slide = $("<div />").addClass("slide"),
    $control = $("<a />").addClass("slider-control"),
    $list = $("<ol />").addClass("table-contents"),
    $item = $("<li />"),
    $link = $("<a />"),
    $bigTitle = $("<h1><span></span></h1>"),
    $title = $("<h2><span></span></h2>"),
    $subTitle = $("<h3><span></span></h3>");

let nameSlide0 = "home",
    hrefSlide0 = "#" + nameSlide0,
    $titleSlide0 = $bigTitle.clone(),
    $slide0,
    hrefPrevSlide = hrefSlide0,
    $prevSlide;

let slidesFile = new Object(),
    subSlidesMap = new Object();

let oDefault = {
    anim: 'slide-in horizontal', // A string : {'slide-in horizontal / vertical'} || 'fade-in' (the side for the slide effect is controlled by the controls)
    bgPattern: 'Cubes', // A string : 'Seigaiha' (waves), 'Rombes' (triangles), 'Cross', 'Dots', 'Check', 'Crossdots' (3 colors best), 'Carbon' (3 colors best), 'Cubes' (3 colors best),  
    bgColors: { // An object containing background colors (main, secondary and third color)
        1: 'var(--color-light)',
        2: '#eae5da',
        3: 'var(--bg-color-2)',
        'title-1': 'var(--color-main)',
        'title-2': 'var(--color-secondary)'
    },
    colors: { // An object containing all colors (PS : you can configure bg colors just by adjusting main colors)
        default: '#333333',
        main: '#003534',
        secondary: '#52B8B5',
        highlight: '#BD0061',
        light: '#F3EADA',
        invalid: '#f94949',
        valid: '#6ad846',
    },
};

let tDefault = 'Full CSS/HTML/JS Presentation';

let o = $.extend({}, oDefault, typeof OPTIONS === 'object' ? OPTIONS : {});

let t = typeof TITLE === 'string' ? TITLE : tDefault;

if ("anim" in o && typeof o.anim === 'string') {
    let sliderAnim = o.anim.toLowerCase();
    let direction = ' ';
    if (sliderAnim.indexOf('slide-in') > -1) {
        if (sliderAnim.indexOf('horizontal') > -1) {
            direction += 'to-left';
        } else if (sliderAnim.indexOf('vertical') > -1) {
            direction += 'to-bottom';
        }
    }

    $($slider).addClass(sliderAnim + direction);
}

if ("bgPattern" in o && typeof o.bgPattern === 'string') {
    $($slider).addClass(o.bgPattern.toLowerCase());
}

if ("bgColors" in o) {
    if (typeof o.bgColors === 'object') {
        $.each(o.bgColors, function (i, color) {
            let prop = '--bg-color-' + i;
            if (color) document.documentElement.style.setProperty(prop, color);
        });
    }
}

if ("colors" in o) {
    if (typeof o.colors === 'object') {
        $.each(o.colors, function (key, color) {
            let prop = '--color-' + key;
            if (color) document.documentElement.style.setProperty(prop, color);
        });
    }
}

$titleSlide0.children("span").append(t);

$(function () {

    // load and construct the slides
    $.ajax({
        async: false,
        type: 'GET',
        url: "slides/",
        success: function (data) {
            $(data).find("td > a").each(function () {
                if (openFile($(this).attr("href"))) {
                    let fileURL = $(this).attr("href");
                    let aKey = fileURL.replace(".html", "").split("-");
                    let key = aKey[1];
                    let iSlide = aKey[0].replace("/slides/", "");
                    if (iSlide.indexOf('.') > -1) {
                        let aParent = iSlide.split("."),
                            iParent = aParent[0];
                        if (!('subSlides' in slidesFile[iParent])) {
                            slidesFile[iParent]['subSlides'] = {};
                        }
                        slidesFile[iParent]['subSlides'][iSlide] = {
                            url: fileURL,
                            key: key,
                        };
                    } else {
                        slidesFile[iSlide] = {
                            url: fileURL,
                            key: key,
                        };
                    }
                }
            });
        },
        complete: function () {

            totalSlides = countMe(slidesFile);
            $.each(slidesFile, function (iSlide, object) {
                let url = object.url,
                    key = object.key;

                let html = '',
                    titleCurSlide = '',
                    contentCurSlide = '',
                    hasSubList = 'subSlides' in object,
                    isEnd = (iSlide >= Object.keys(slidesFile).length),
                    slideClasses = '';
                $.ajax({
                    async: false,
                    type: 'GET',
                    url: url,
                    success: function (data) {
                        html = $.parseHTML(data);
                        $.each(html, function (i, el) {
                            switch (el.nodeName) {
                                case 'TITLE':
                                    titleCurSlide = decodeEntities($(el).html());
                                    break;
                                case 'CLASSES':
                                    slideClasses = $(el).text();
                                    break;
                                case 'CONTENT':
                                    contentCurSlide = $(el).contents();
                                    break;
                            }
                        });
                        if (slideCount == 0) {
                            // Prepares first slide with title and table of contents
                            let nameSlide1 = key,
                                hrefSlide1 = "#" + nameSlide1;

                            $slide0 = $slide.clone()
                                .attr("id", nameSlide0)
                                .addClass("text-center")
                                .append(
                                    $titleSlide0,
                                    $list.clone(),
                                    $control.clone().addClass("next").attr("href", hrefSlide1)
                                );
                            $("title").append(t);
                            $($slider).append(
                                $slide0,
                                "<!-- \\\\ SLIDE 0 // -->"
                            );
                            $prevSlide = $(hrefSlide0);
                        }
                        slideCount = iSlide;

                        let nameCurSlide = key,
                            hrefCurSlide = "#" + nameCurSlide,
                            isBig = slideClasses.indexOf('full-title') !== -1,
                            $titleCurSlide = hasSubList ? $title.clone() : (isBig ? $bigTitle.clone() : $subTitle.clone());

                        $titleCurSlide.children("span").html(titleCurSlide);

                        // Fill main list table contents
                        $slide0.children('.table-contents').append(
                            $item.clone().append(
                                $link.clone().html(titleCurSlide).attr("href", hrefCurSlide)
                            )
                        );
                        // Create new slide
                        let $curSlide = $slide.clone()
                            .attr("id", nameCurSlide)
                            .addClass(slideClasses)
                            .append(
                                $titleCurSlide,
                                (hasSubList ? $list.clone() : contentCurSlide),
                                $control.clone().addClass("prev").attr("href", hrefPrevSlide)
                            );

                        // Add next control to previous slide and prepend new slide before previous slide
                        $prevSlide.append(
                            $control.clone().addClass("next").attr("href", hrefCurSlide)
                        ).before(
                            $curSlide,
                            "<!-- \\\\ SLIDE " + iSlide + " // -->"
                        );

                        $prevSlide = $curSlide;
                        hrefPrevSlide = hrefCurSlide;
                        progress();

                        if (hasSubList) {
                            let $parentSlide = $curSlide,
                                subTable = object.subSlides;
                            $.each(subTable, function (iSubSlide, subObj) {
                                let subUrl = subObj.url,
                                    subKey = subObj.key;

                                let subTitle = '',
                                    subSlideClasses = '',
                                    subContent = '';

                                $.ajax({
                                    async: false,
                                    type: 'GET',
                                    url: subUrl,
                                    success: function (data) {
                                        html = $.parseHTML(data);
                                        $.each(html, function (i, el) {
                                            switch (el.nodeName) {
                                                case 'TITLE':
                                                    subTitle = $(el).text();
                                                    break;
                                                case 'CLASSES':
                                                    subSlideClasses = $(el).text();
                                                    break;
                                                case 'CONTENT':
                                                    subContent = $(el).contents();
                                                    break;
                                            }
                                        });

                                        let subName = subKey,
                                            subHref = "#" + subName,
                                            subIsBig = subSlideClasses.indexOf('full-title') !== -1,
                                            $subTitleCurSlide = subIsBig ? $bigTitle.clone() : $subTitle.clone();

                                        if (subIsBig) {
                                            $subTitleCurSlide.children("span").html(subTitle);
                                        } else {
                                            $subTitleCurSlide.children("span").append(
                                                $link.clone().attr("href", hrefCurSlide).html(titleCurSlide),
                                                " > ",
                                                "<small>" + subTitle + "</small>")
                                        }

                                        // Fill cur sublist table contents
                                        $parentSlide.children('.table-contents').append(
                                            $item.clone().append(
                                                $link.clone().append(subTitle).attr("href", subHref)
                                            )
                                        );

                                        let $subSlide = $slide.clone()
                                            .attr("id", subName)
                                            .addClass(subSlideClasses)
                                            .append(
                                                $subTitleCurSlide,
                                                subContent,
                                                $control.clone().addClass("prev").attr("href", hrefPrevSlide)
                                            );

                                        // Highlight syntax if possible
                                        if ($subSlide.find('code[class*="language-"]').length) {
                                            let $codeEls = $subSlide.find('code[class*="language-"]'),
                                                subString = "language-";
                                            $codeEls.each(function () {
                                                let code = $(this).get(0);
                                                let lang = $(this).attr('class').substr(subString.length);
                                                var newCode = document.createElement('code');
                                                newCode.textContent = code.textContent;
                                                newCode.className = code.className;
                                                loadLanguage(lang).then(function () {
                                                    Prism.highlightElement(newCode);
                                                });
                                                code.parentElement.replaceChild(newCode, code);
                                            });
                                        }

                                        $prevSlide.append(
                                            $control.clone().addClass("next").attr("href", subHref)
                                        ).before(
                                            $subSlide,
                                            "<!-- \\\\ SLIDE " + iSubSlide + " // -->"
                                        );

                                        $prevSlide = $subSlide;
                                        hrefPrevSlide = subHref;
                                        progress();
                                    }
                                });
                            });
                        }
                    }
                });
            });
        }
    });

    // shortcuts
    $("body").on('keydown', function (e) {
        let modifier = e.shiftKey || e.metaKey || e.ctrlKey || e.altKey;
        // previous
        if (e.code == 'ArrowLeft' && !modifier) {
            sc__prevSlide();
        }
        // next
        else if (e.code == 'ArrowRight' && !modifier) {
            sc__nextSlide();
        }
        // fullscreen
        else if (e.code == 'KeyF' && !modifier) {
            sc__enterFS();
        }
        // home
        else if (e.code == 'KeyH' && !modifier) {
            sc__homeSlide();
        }
        // spacebar (next+sections)
        else if (e.code == 'Space' && !modifier) {
            sc__nextPart();
        }
        // MAJ spacebar (prev+sections)
        else if (e.code == 'Space' && e.shiftKey) {
            sc__prevPart();
        }
    });

    // click slider controls
    $(document).on('click', '.slider-control,.slider-control-home', function (e) {
        // make sure home button has default color
        $('.slider-control-home').children('svg').addClass('text-light');
        if ($slider.hasClass('slide-in')) {
            if ($(this).hasClass('next')) {
                if ($slider.hasClass('horizontal')) {
                    $slider.removeClass('to-right').addClass('to-left');
                } else if ($slider.hasClass('vertical')) {
                    $slider.removeClass('to-top').addClass('to-bottom');
                }
            } else if ($(this).hasClass('prev') || $(this).hasClass('slider-control-home')) {
                if ($slider.hasClass('horizontal')) {
                    $slider.removeClass('to-left').addClass('to-right');
                } else if ($slider.hasClass('vertical')) {
                    $slider.removeClass('to-bottom').addClass('to-top');
                }
            }
        }
    });

    // handle scroll to top
    if ("onhashchange" in window) {
        window.onhashchange = function () {
            $(".slide:visible").slideScrollHandler().scrollTop(0);
        }
    }

    $(".slide:visible").slideScrollHandler().scrollTop(0);
});

$.fn.hasScrollBar = function () {
    return this.get(0).scrollHeight > this.get(0).offsetHeight;
};
$.fn.slideScrollHandler = function () {
    if ($(this).hasScrollBar()) {
        $(this).addClass('scrollable scroll-indicator');

        $(this).on('scroll', function () {
            if ($(this).get(0).scrollTop > 40) {
                $('.slider-control-home').children('svg').removeClass('text-light')
                $(this).removeClass('scroll-indicator');
            } else {
                $('.slider-control-home').children('svg').addClass('text-light')
            }
        });
    }
    return $(this);
};
function openFile(file) {
    let extension = file.substr((file.lastIndexOf('.') + 1));
    switch (extension) {
        case 'html':
            return true;
            break;
        default:
            return false;
    }
};
function countMe(obj) {
    let count = 0;
    $.each(obj, function (iParent, parent) {
        if ('url' in parent) {
            count++;
        }
        if ('subSlides' in parent) {
            $.each(parent['subSlides'], function (iSlide, slide) {
                if ('url' in slide) {
                    count++;
                }
            });
        }
    });
    return count;
};
function progress() {
    loadingProgress++;
    let progressWidth = (loadingProgress / totalSlides * 100);
    $('.progress').width(progressWidth + '%');
    if (progressWidth == 100) {
        $('.progress-text-loading').fadeOut(200);
        $('.progress-text-complete').fadeIn(200);
        $('.progress-wrap').delay(4000).fadeOut(500);
        actionsPostLoading();
    }
};
function actionsPostLoading() {
    $($slider).fadeIn(200);
};
function sc__enterFS() {
    let elem = document.documentElement;
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
};
function sc__prevSlide() {
    let strPrevBtn = '.slider-control.prev:visible';
    if ($slider.find(strPrevBtn)[0]) {
        $slider.find(strPrevBtn)[0].click();
    }
};
function sc__nextSlide() {
    let strNextBtn = '.slider-control.next:visible';
    if ($slider.find(strNextBtn)[0]) {
        $slider.find(strNextBtn)[0].click();
    }
};
function sc__homeSlide() {
    let strHomeBtn = '.slider-control-home';
    if ($(document).find(strHomeBtn)[0]) {
        $(document).find(strHomeBtn)[0].click();
    }
};
function sc__nextPart() {
    let $slideON = $('.slide:visible'),
        offset = 100,
        curScroll = $slideON.get(0).scrollTop,
        fullHeight = $slideON.get(0).scrollHeight,
        viewPort = $slideON.get(0).offsetHeight;

    if (viewPort + curScroll < fullHeight - offset) {
        $slideON.animate({
            scrollTop: curScroll + viewPort
        }, 200);
    } else {
        sc__nextSlide();
    }
};
function sc__prevPart() {
    let $slideON = $('.slide:visible'),
        curScroll = $slideON.get(0).scrollTop,
        viewPort = $slideON.get(0).offsetHeight;

    if (curScroll > 0) {
        $slideON.animate({
            scrollTop: curScroll - viewPort
        }, 200);
    } else {
        sc__prevSlide();
    }
};
function decodeEntities(input) {
    var y = document.createElement('textarea');
    y.innerHTML = input;
    return y.value;
};
/**
 * Loads a language, including all dependencies
 *
 * @param {string} lang the language to load
 * @type {Promise} the promise which resolves as soon as everything is loaded
 */
function loadLanguage(lang) {
    // at first we need to fetch all dependencies for the main language
    // Note: we need to do this, even if the main language already is loaded (just to be sure..)
    //
    // We load an array of all dependencies and call recursively this function on each entry
    //
    // dependencies is now an (possibly empty) array of loading-promises
    var dependencies = getDependenciesOfLanguage(lang).map(loadLanguage);

    // We create a promise, which will resolve, as soon as all dependencies are loaded.
    // They need to be fully loaded because the main language may extend them.
    return Promise.all(dependencies)
        .then(function () {

            // If the main language itself isn't already loaded, load it now
            // and return the newly created promise (we chain the promises).
            // If the language is already loaded, just do nothing - the next .then()
            // will immediately be called
            if (!Prism.languages[lang]) {
                return new Promise(function (resolve) {
                    let script = document.createElement("script");
                    script.src = 'node_modules/prismjs/components/prism-' + lang + '.js';
                    script.async = true;
                    script.onload = resolve;
                    document.body.appendChild(script);
                });
            }
        });
};

/**
 * Returns all dependencies (as identifiers) of a specific language
 *
 * @param {string} lang
 * @returns {Array.<string>} the list of dependencies. Empty if the language has none.
 */
function getDependenciesOfLanguage(lang) {
    if (!components.languages[lang] || !components.languages[lang].require) {
        return [];
    }

    return (typeof (components.languages[lang].require) === "array")
        ? components.languages[lang].require
        : [components.languages[lang].require];
};
