let mix = require('laravel-mix');

require('laravel-mix-purgecss');

mix.options({
  processCssUrls: false,
});

mix.postCss('pcss/tailwind.pcss', 'css/stylesheet.css', [
  require('tailwindcss'),
  require('postcss-nested')
]);

mix.purgeCss({
  enabled: false,
  folders: ['.', 'pcss', 'slides'],
  extensions: ['html', 'php', 'twig', 'pcss'],
  whitelistPatterns: [],
  whitelistPatternsChildren: []
});
